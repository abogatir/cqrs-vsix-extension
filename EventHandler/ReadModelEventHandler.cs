﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace $rootnamespace$
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IAsyncEventHandler{$fileinputname$}" />
    [ReadModelEventHandling]
    public class $fileinputname$ReadModelEventHandler : IAsyncEventHandler<$fileinputname$>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="$fileinputname$" /> class.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public $fileinputname$ReadModelEventHandler()
        {
        }

        /// <summary>
        /// Handles the specified event asynchronously.
        /// </summary>
        /// <param name="event">The event to handle.</param>
        public async Task HandleAsync($fileinputname$ @event)
        {
            
        }
    }
}
