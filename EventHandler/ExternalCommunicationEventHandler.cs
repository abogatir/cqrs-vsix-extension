﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace $rootnamespace$
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IAsyncEventHandler{$fileinputname$}" />
    [ExternalCommunicationEventHandling]
    public class $fileinputname$ExternalCommunicationEventHandler : IAsyncEventHandler<$fileinputname$>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="$fileinputname$" /> class.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public $fileinputname$ExternalCommunicationEventHandler()
        {
        }

        /// <summary>
        /// Handles the specified event asynchronously.
        /// </summary>
        /// <param name="event">The event to handle.</param>
        public async Task HandleAsync($fileinputname$ @event)
        {
            
        }
    }
}
