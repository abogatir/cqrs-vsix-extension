useful links for developing vsix projects

DONT FORGET TO SIGN ASSEMBLIES THAT SERVE AS WIZARDS

https://msdn.microsoft.com/en-us/library/ff728613.aspx - publishing extension
https://msdn.microsoft.com/en-us/library/ms247115.aspx?f=255&MSPPError=-2147217396 - multi item template
https://msdn.microsoft.com/en-us/library/ff407026.aspx - VSIX Deployment
https://msdn.microsoft.com/en-us/library/eehb4faa(v=vs.100).aspx - template parameters
https://msdn.microsoft.com/en-us/library/ms247063%28v=vs.100%29.aspx?f=255&MSPPError=-2147217396 custom parameters
https://msdn.microsoft.com/en-us/library/y3kkate1(v=vs.100).aspx - locate and organize
https://msdn.microsoft.com/en-us/library/ms185311(v=vs.100).aspx - subs params
https://www.codeproject.com/articles/365680/extending-visual-studio-part-item-templates - guide
https://blogs.endjin.com/2014/07/a-step-by-step-guide-to-developing-visual-studio-item-templates-using-sidewaffle/ - guide
https://blogs.msdn.microsoft.com/vsx/2014/06/10/creating-a-vsix-deployable-project-or-item-template-with-custom-wizard-support/ - guide

https://channel9.msdn.com/Events/Build/2016/B886 - definitive guide to extensibility
