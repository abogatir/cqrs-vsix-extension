﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace $rootnamespace$
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="IAsyncCommandHandler{$fileinputname$}" />
    public class $fileinputname$CommandHandler : IAsyncCommandHandler<$fileinputname$>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="$fileinputname$" /> class.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public $fileinputname$CommandHandler()
        {
        }

        /// <summary>
        /// Handles the specified command asynchronously.
        /// </summary>
        /// <param name="command">The command to handle.</param>
        public async Task HandleAsync($fileinputname$ command)
        {
            
        }
    }
}
