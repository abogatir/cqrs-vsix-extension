﻿using System.Collections.Generic;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;

namespace HandlerWizard
{
    public class HandlerWizard: IWizard
    {
        private List<string> _includedFiles;
        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            var form = new WizardForm();
            form.ShowDialog();
            _includedFiles = new List<string>();

            if (form.CbDomainEventHandler.Checked) _includedFiles.Add("DomainEventHandler.cs");
            if (form.CbReadModelEventHandler.Checked) _includedFiles.Add("ReadModelEventHandler.cs");
            if (form.CbExternalCommunicationEventHandler.Checked) _includedFiles.Add("ExternalCommunicationEventHandler.cs");
        }        
        public void ProjectFinishedGenerating(Project project) {}

        public void ProjectItemFinishedGenerating(ProjectItem projectItem){}

        public bool ShouldAddProjectItem(string filePath)
        {
            return _includedFiles.Contains(filePath);
        }

        public void BeforeOpeningFile(ProjectItem projectItem) {}

        public void RunFinished() {}
    }
}
