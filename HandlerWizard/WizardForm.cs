﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HandlerWizard
{
    public partial class WizardForm : Form
    {
        public WizardForm()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (CbReadModelEventHandler.Checked || CbDomainEventHandler.Checked ||
                CbExternalCommunicationEventHandler.Checked)
            {
                Close();
            }
            else
            {
                MessageBox.Show("No handlers selected!");
            }
        }
    }
}
