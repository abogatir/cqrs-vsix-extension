﻿using System.Windows.Forms;

namespace HandlerWizard
{
    partial class WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CbReadModelEventHandler = new System.Windows.Forms.CheckBox();
            this.CbDomainEventHandler = new System.Windows.Forms.CheckBox();
            this.CbExternalCommunicationEventHandler = new System.Windows.Forms.CheckBox();
            this.BtnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CbReadModelEventHandler
            // 
            this.CbReadModelEventHandler.AutoSize = true;
            this.CbReadModelEventHandler.Checked = true;
            this.CbReadModelEventHandler.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbReadModelEventHandler.Location = new System.Drawing.Point(22, 13);
            this.CbReadModelEventHandler.Name = "CbReadModelEventHandler";
            this.CbReadModelEventHandler.Size = new System.Drawing.Size(155, 17);
            this.CbReadModelEventHandler.TabIndex = 0;
            this.CbReadModelEventHandler.Text = "Read Model Event Handler";
            this.CbReadModelEventHandler.UseVisualStyleBackColor = true;
            // 
            // CbDomainEventHandler
            // 
            this.CbDomainEventHandler.AutoSize = true;
            this.CbDomainEventHandler.Location = new System.Drawing.Point(22, 51);
            this.CbDomainEventHandler.Name = "CbDomainEventHandler";
            this.CbDomainEventHandler.Size = new System.Drawing.Size(133, 17);
            this.CbDomainEventHandler.TabIndex = 1;
            this.CbDomainEventHandler.Text = "Domain Event Handler";
            this.CbDomainEventHandler.UseVisualStyleBackColor = true;
            // 
            // CbExternalCommunicationEventHandler
            // 
            this.CbExternalCommunicationEventHandler.AutoSize = true;
            this.CbExternalCommunicationEventHandler.Location = new System.Drawing.Point(22, 88);
            this.CbExternalCommunicationEventHandler.Name = "CbExternalCommunicationEventHandler";
            this.CbExternalCommunicationEventHandler.Size = new System.Drawing.Size(210, 17);
            this.CbExternalCommunicationEventHandler.TabIndex = 2;
            this.CbExternalCommunicationEventHandler.Text = "External Communication Event Handler";
            this.CbExternalCommunicationEventHandler.UseVisualStyleBackColor = true;
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(475, 229);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(75, 23);
            this.BtnOk.TabIndex = 3;
            this.BtnOk.Text = "OK";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // WizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 264);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.CbExternalCommunicationEventHandler);
            this.Controls.Add(this.CbDomainEventHandler);
            this.Controls.Add(this.CbReadModelEventHandler);
            this.Name = "WizardForm";
            this.Text = "Event Handler Template Selection";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public CheckBox CbReadModelEventHandler;
        public CheckBox CbDomainEventHandler;
        public CheckBox CbExternalCommunicationEventHandler;
        public Button BtnOk;
    }
}